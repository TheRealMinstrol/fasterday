package me.Minstrol.FasterDay;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin  {

    World world = Bukkit.getWorld("world");

    @Override
    public void onEnable() {
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                long time = world.getTime();
                world.setTime(time + 1);
            }
        },0L, 1L);
    }
}
